'use strict';

const express = require('express');
const dotenv = require('dotenv');
const exphbs = require('express-handlebars');
const cookieSession = require('cookie-session');
const flash = require('connect-flash');
const path = require('path');
const morgan = require('morgan');
const mongoose = require('mongoose');
const routes = require('./routes');

const app = express();
const port = process.env.PORT || 3000;
dotenv.config();

app.set('trust proxy', 1);
app.use(cookieSession({
  name: 'dsid',
  secret: process.env.APP_KEY,
  sameSite: 'lax',
}));
app.use(flash());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.urlencoded({ extended: true }));
app.use(morgan('dev'));
app.use(routes);

mongoose.connect(process.env.MONGO_URL)
  .then(() => console.log('Connected correctly to server.'))
  .catch((err) => console.log(err));

app.engine('.hbs', exphbs({
  extname: '.hbs',
  helpers: {
    appName: () => process.env.APP_NAME,
  },
}));
app.set('view engine', '.hbs');

app.listen(port, () => console.log(`Listening on port ${port}`));
