function ready(fn) {
  if (document.readyState !== 'loading') {
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}

ready(function () {
  const form = document.querySelector('form');

  form.addEventListener('submit', function () {
    const button = form.querySelector('button[type="submit"]');
    button.setAttribute('disabled', true);
    button.textContent = 'Sending...';
  }, false);
});