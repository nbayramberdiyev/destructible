'use strict';

const express = require('express');
const csrf = require('csurf');
const nodemailer = require('nodemailer');
const Message = require('../models/message');

const router = express.Router();

router.get('/', csrf(), function (req, res) {
  res.render('home', {
    csrf: req.csrfToken(),
    flash: {
      success: req.flash('success'),
      error: req.flash('error'),
    },
  });
});

router.post('/', csrf(), function (req, res, next) {
  const transporter = nodemailer.createTransport({
    host: process.env.MAIL_HOST,
    port: process.env.MAIL_PORT,
    secure: process.env.MAIL_ENCRYPTION === 'ssl',
    auth: {
      user: process.env.MAIL_USERNAME,
      pass: process.env.MAIL_PASSWORD,
    },
  });

  Message.create({ content: req.body.message })
    .then((message) => {
      const url = `${req.protocol}://${req.headers.host}/messages/${message._id}`;

      transporter.sendMail({
        from: `${process.env.MAIL_FROM_NAME} <${process.env.MAIL_FROM_ADDRESS}>`,
        to: req.body.email,
        subject: 'A new self destructing message',
        text: 'You have a new self-destructing message. Click the following link to view: ' + url,
        html: '<p>You have a new self-destructing message.</p><p>Click <a href="' + url + '">here</a> to view.</p>',
      }, (error, info) => {
        if (error) {
          console.log(error);
          req.flash('error', 'Your message was unable to send.');
        } else {
          req.flash('success', 'Your message was sent successfully.');
        }

        res.redirect('back');
      });
    })
    .catch((err) => next(err));
});

router.get('/messages/:messageId', function (req, res, next) {
  Message.findByIdAndRemove(req.params.messageId, (err, message) => {
    if (err) {
      return next(err);
    }

    if (!message) {
      const err = new Error('Message could not be found.');
      err.status = 404;
      return next(err);
    }

    res.render('message', {
      message: message.toJSON(),
    });
  });
});

module.exports = router;
