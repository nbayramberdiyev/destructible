const mongoose = require('mongoose');
const { config } = require('dotenv');
const mongooseFieldEncryption = require('mongoose-field-encryption').fieldEncryption;
const Schema = mongoose.Schema;

config();

const messageSchema = new Schema({
  content: {
    type: String,
    required: true,
  },
}, {
  timestamps: true,
});

messageSchema.plugin(mongooseFieldEncryption, { fields: ['content'], secret: process.env.APP_KEY });

const Message = mongoose.model('Message', messageSchema);

module.exports = Message;
